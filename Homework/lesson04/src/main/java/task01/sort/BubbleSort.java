package task01.sort;

import task01.model.Person;

public class BubbleSort implements Sort {
    private final String sortName;

    public BubbleSort() {
        this.sortName = "Сортировка пузырьком";
    }

    public Person[] sort(Person[] people) {
        Person[] copyPeople = people.clone();
        if (copyPeople != null) {
            for (int i = 0; i < copyPeople.length; i++) {
                for (int j = 0; j < copyPeople.length - i - 1; j++) {
                    if (copyPeople[j].compareTo(copyPeople[j + 1]) > 0) {
                        Person copyPerson = copyPeople[j];
                        copyPeople[j] = copyPeople[j + 1];
                        copyPeople[j + 1] = copyPerson;
                    }
                }
            }
        }
        return copyPeople;
    }

    @Override
    public String getSortName() {
        return sortName;
    }
}
