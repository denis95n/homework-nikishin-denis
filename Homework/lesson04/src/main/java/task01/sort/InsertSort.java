package task01.sort;

import task01.model.Person;


public class InsertSort implements Sort {
    private final String sortName;

    public InsertSort() {
        sortName = "Сортировка вставками";
    }


    @Override
    public Person[] sort(Person[] people) {
        Person[] copyPeople = people.clone();
        if (copyPeople != null) {
            Person copyPerson;
            for (int i = 0; i < copyPeople.length; i++) {
                copyPerson = copyPeople[i];
                int j = i - 1;
                for (; j >= 0; --j) {
                    if (copyPerson.compareTo(copyPeople[j]) < 0) {
                        copyPeople[j + 1] = copyPeople[j];
                    } else break;
                }
                copyPeople[j + 1] = copyPerson;
            }
        }

        return copyPeople;
    }

    @Override
    public String getSortName() {
        return sortName;
    }
}
