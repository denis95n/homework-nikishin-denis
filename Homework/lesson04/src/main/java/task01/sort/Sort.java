package task01.sort;

import task01.model.Person;

public interface Sort {
    Person[] sort(Person[] people);

    String getSortName();
}
