package task01.utils;

import task01.model.Person;

import java.util.ArrayList;
import java.util.List;

public class Duplicate {
    public static void checkDuplicateAndPrint(Person[] people) {
        List<Person> duplicate = new ArrayList<>();

        for (int i = 0; i < people.length; i++) {
            for (int j = i + 1; j < people.length; j++) {
                if (people[i].equals(people[j])) {
                    duplicate.add(people[i]);
                }
            }
        }

        if (duplicate.size() > 0) {
            printDuplicate(duplicate);
        }
    }

    private static void printDuplicate(List<Person> duplicate) {
        System.out.println("######################################################################################\n" +
                "Дубликаты: " + duplicate.size());
        duplicate.forEach(System.out::println);
        System.out.println("######################################################################################");
    }

}
