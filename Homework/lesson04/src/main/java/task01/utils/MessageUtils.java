package task01.utils;

import task01.model.Person;

import java.util.Arrays;

public class MessageUtils {
    public static void showSortStat(Person[] people, String sortName, long timeEndLoad) {
        System.out.println("-------------------------------------------\n" +
                sortName + "\n" +
                "Отсортированный список:");
        Arrays.stream(people).forEach(System.out::println);
        System.out.println("###########################################\n" +
                sortName + "\n" +
                "Количество записей:" + people.length + "\n" +
                "Время выполнения:" + timeEndLoad + "ms\n" +
                "-------------------------------------------");
    }
}
