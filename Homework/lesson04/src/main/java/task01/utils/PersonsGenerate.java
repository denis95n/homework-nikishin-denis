package task01.utils;

import task01.model.Person;
import task01.model.Sex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static task01.data.PersonArray.listOfManNames;
import static task01.data.PersonArray.listOfWomanNames;

public class PersonsGenerate {
    private Person[] people;
    private int countGenerateEntry;

    public PersonsGenerate(int countGenerateEntry) {
        this.countGenerateEntry = countGenerateEntry;
        people = new Person[countGenerateEntry];
    }

    public Person[] generate() {
        Random random = new Random();
        int countGenerateMan = random.nextInt(countGenerateEntry);
        Sex manSex = new Sex(Sex.MAN);
        Sex womanSex = new Sex(Sex.WOMAN);
        Person newPerson;

        for (int i = 0; i < countGenerateMan; i++) {
            newPerson = new Person(random.nextInt(101), manSex,
                    listOfManNames.get(random.nextInt(listOfManNames.size())));
            people[i] = newPerson;
        }

        for (int i = countGenerateMan; i < countGenerateEntry; i++) {
            newPerson = new Person(random.nextInt(101), womanSex,
                    listOfWomanNames.get(random.nextInt(listOfWomanNames.size())));
            people[i] = newPerson;
        }

        return people;
    }
}
