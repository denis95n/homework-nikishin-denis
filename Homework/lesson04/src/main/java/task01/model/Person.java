package task01.model;

import java.util.Objects;

public class Person implements Comparable<Person> {
    private int age;
    private Sex sex;
    private String name;

    public Person(int age, Sex sex, String name) {
        if (age >= 0 && age <= 100) {
            this.age = age;
        } else throw new IllegalArgumentException("Неверный возраст!");
        this.sex = sex;
        this.name = name;
    }

    @Override
    public int compareTo(Person o) {
        int result;
        if (sex.equals(o.sex)) {
            result = compareAgeTo(o.age);
            if (result == 0) {
                result = compareNameTo(o.name);
            }
        } else if (sex.isMan()) {
            result = -1;
        } else result = 1;

        return result;
    }

    private int compareNameTo(String name) {
        return this.name.compareTo(name);
    }

    private int compareAgeTo(int age) {
        int result = 0;
        if (this.age > age) result = -1;
        else if (this.age < age) result = 1;
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", sex='" + sex + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(sex, person.sex) &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, sex, name);
    }
}