package task01.model;

public final class Sex {
    public static final String MAN = "man";
    public static final String WOMAN = "woman";
    private String sex;

    public Sex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public boolean equals (Sex sex){
        boolean isEquals = false;
        if (sex.getSex().equals(this.sex) ){
            isEquals = true;
        }
        return isEquals;
    }

    public boolean isMan (){
        return this.getSex().equals(MAN);
    }

    @Override
    public String toString() {
        return "Sex{" +
                "sex='" + sex + '\'' +
                '}';
    }
}
