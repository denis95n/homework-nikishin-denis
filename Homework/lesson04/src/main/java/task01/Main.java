package task01;

import task01.model.Person;
import task01.sort.BubbleSort;
import task01.sort.InsertSort;
import task01.sort.Sort;
import task01.utils.Duplicate;
import task01.utils.MessageUtils;
import task01.utils.PersonsGenerate;

import java.time.Duration;
import java.time.Instant;

public class Main {
    public static void main(String[] args) {
        PersonsGenerate personsGenerate = new PersonsGenerate(10000);
        Person[] generatePerson = personsGenerate.generate();
        Duplicate.checkDuplicateAndPrint(generatePerson);

        BubbleSort bubbleSort = new BubbleSort();
        InsertSort insertSort = new InsertSort();

        sort(bubbleSort, generatePerson);
        sort(insertSort, generatePerson);
    }


    public static void sort(Sort typeSort, Person[] people) {
        Instant start = Instant.now();
        Person[] peopleSortedBubble = typeSort.sort(people);
        Instant finish = Instant.now();
        MessageUtils.showSortStat(peopleSortedBubble, typeSort.getSortName(),
                Duration.between(start, finish).toMillis());
    }
}
