package task01.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    @Test
    void compareTo() {
        Person valera = new Person(1, new Sex(Sex.MAN), "Валера");
        Person alina = new Person(100, new Sex(Sex.WOMAN), "Alina");

        assertTrue(valera.compareTo(alina) < 0);
    }
}