package task01.sort;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import task01.model.Person;
import task01.utils.PersonsGenerate;

import java.util.Arrays;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertTrue;


class SortTest {
    private Person[] generatePerson;

    @BeforeEach
    void setUp() {
        generatePerson = new PersonsGenerate(10000).generate();
    }

    @Test
    void sortToBubble() {
        Person[] expectedArray = generatePerson.clone();
        Arrays.sort(expectedArray);

        Person[] sortToBubble = new BubbleSort().sort(generatePerson);

        assertTrue(Objects.deepEquals(sortToBubble, expectedArray));
    }

    @Test
    void sortToInsert() {
        Person[] expectedArray = generatePerson.clone();
        Arrays.sort(expectedArray);

        Person[] sortToInsert = new InsertSort().sort(generatePerson);

        assertTrue(Objects.deepEquals(sortToInsert, expectedArray));
    }
}