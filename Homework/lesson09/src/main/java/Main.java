import server.Server;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            String directory = scanner.nextLine();
            new Server(directory).start();
        }
    }
}
