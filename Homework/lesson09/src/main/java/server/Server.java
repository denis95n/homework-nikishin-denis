package server;

import client.Client;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private static final int PORT = 1500;
    private String directory;

    public Server(String directory) {
        this.directory = directory;
    }

    public void start(){
        try(ServerSocket serverSocket = new ServerSocket(PORT)){
            System.out.println("Сервер запущен");
            while (true) {
                Socket clientSocket = serverSocket.accept();
                Client client = new Client(clientSocket, directory);
                client.start();
            }

        } catch (IOException ex){
            ex.printStackTrace();
        }
    }
}
