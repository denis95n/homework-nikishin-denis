package response;

public enum StatusCode {
    OK("Ок", 200), NOT_FOUND("Not found", 404);

    private String message;
    private int code;

    StatusCode(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
