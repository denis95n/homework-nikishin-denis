import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;

public class MyThreadPool implements Executor {
    private final Queue<Runnable> tasks = new ConcurrentLinkedQueue<>();
    private boolean isRun = true;

    public MyThreadPool(int countThreads) {
        for (int i = 0; i < countThreads; i++) {
            new Thread(new TaskWorker()).start();
        }
    }

    @Override
    public void execute(Runnable command) {
        if (isRun) {
            tasks.add(command);
        }
    }

    public void stop() {
        isRun = false;
    }

    private final class TaskWorker implements Runnable {

        @Override
        public void run() {
            while (isRun) {
                Runnable nextTask = tasks.poll();
                if (nextTask != null) {
                    System.out.println(nextTask.toString());
                    nextTask.run();
                }
            }
        }
    }
}
