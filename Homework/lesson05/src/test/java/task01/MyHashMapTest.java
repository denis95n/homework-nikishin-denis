package task01;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class MyHashMapTest {
    private HashMap<Integer, Integer> assertHashMap = new HashMap<>();
    private MyHashMap<Integer, Integer> myHashMap = new MyHashMap<>();

    @BeforeEach
    void setUp() {
        assertHashMap.put(1, 1);
        assertHashMap.put(2, 2);
        assertHashMap.put(3, 3);
        assertHashMap.put(4, 4);
        myHashMap.put(1, 1);
        myHashMap.put(2, 2);
        myHashMap.put(3, 3);
        myHashMap.put(4, 4);
    }

    @Test
    void size() {
        assertEquals(assertHashMap.size(), myHashMap.size());
    }

    @Test
    void containsKey() {
        assertEquals(assertHashMap.containsKey(1), myHashMap.containsKey(1));
    }

    @Test
    void containsValue() {
        assertEquals(assertHashMap.containsKey(2), myHashMap.containsKey(2));
    }

    @Test
    void get() {
        assertEquals(assertHashMap.get(1), myHashMap.get(1));
    }

    @Test
    void remove() {
        assertHashMap.remove(2);
        myHashMap.remove(2);
        assertEquals(assertHashMap.containsKey(2), myHashMap.containsKey(2));
    }

    @Test
    void put() {
        assertEquals(assertHashMap, myHashMap);
    }

    @Test
    void clear() {
        assertHashMap.clear();
        myHashMap.clear();

        assertEquals(assertHashMap, myHashMap);
    }
}