package task01;

import java.util.*;

public class MyHashMap<K, V> implements Map<K, V> {

    private int size;
    private Node<K, V>[] table;
    private static final int DEFAULT_INITIAL_CAPACITY = 16;

    static class Node<K, V> implements Entry<K, V> {
        private final int hash;
        private final K key;
        private V value;
        private Node<K, V> next;

        Node(int hash, K key, V value, Node<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public V setValue(V value) {
            this.value = value;
            return value;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this)
                return true;
            if (o instanceof Map.Entry) {
                Entry<?, ?> e = (Entry<?, ?>) o;
                if (key.equals(e.getKey()) && value.equals(e.getValue()))
                    return true;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return key.hashCode() ^ value.hashCode();
        }
    }

    private Node<K, V> newNode(int hash, K key, V value, Node<K, V> next) {
        return new Node<>(hash, key, value, next);
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean containsKey(Object key) {
        return getNode(key) != null;
    }

    public boolean containsValue(Object value) {
        if (table != null) {
            if (size > 0) {
                for (int i = 0; i < table.length; ++i) {
                    for (Node<K, V> node = table[i]; node != null; node = node.next) {
                        if (Objects.equals(value, node.value))
                            return true;
                    }
                }
            }
        }
        return false;
    }

    public V get(Object key) {
        Node<K, V> node = getNode(key);
        return node == null ? null : node.value;

    }

    final Node<K, V> getNode(Object key) {
        int hash = hash(key);
        if (table != null) {
            int tableLength = table.length;
            Node<K, V> first = table[(tableLength - 1) & hash];
            if (first != null) {
                if (first.hash == hash && first.key == key || (key != null && key.equals(first.key))) {
                    return first;
                }
                Node<K, V> nextNode;
                if (first.next != null) {
                    nextNode = first.next;
                    while (nextNode != null) {
                        if (nextNode.hash == hash && nextNode.key == key || (key != null && key.equals(nextNode.key))) {
                            return nextNode;
                        }
                        nextNode = nextNode.next;
                    }
                }
            }
        }
        return null;
    }

    public V remove(Object key) {
        int hash = hash(key);
        if (table != null) {
            int tableLength = table.length;
            int index = (tableLength - 1) & hash;
            if (table[index] != null) {
                Node<K, V> currentNode = table[(tableLength - 1) & hash];
                if (currentNode.hash == hash && currentNode.key == key ||
                        (key != null && key.equals(currentNode.key))) {
                    table[index] = currentNode.next;
                    --size;
                    return currentNode.value;
                } else {
                    Node<K, V> nextNode;
                    if (currentNode.next != null) {
                        nextNode = currentNode.next;
                        while (nextNode != null) {
                            if (nextNode.hash == hash && nextNode.key == key ||
                                    (key != null && key.equals(nextNode.key))) {
                                currentNode.next = nextNode.next;
                                return nextNode.value;
                            }
                            currentNode = nextNode;
                            nextNode = nextNode.next;
                        }
                    }
                }
            }
        }
        return null;
    }

    public V put(K key, V value) {
        int hash = hash(key);
        int tableLength, index;
        Node<K, V> currentNode;
        if (table == null) {
            table = resize();
        }
        tableLength = table.length;
        index = (tableLength - 1) & hash;

        if ((currentNode = table[index]) == null) {
            table[index] = newNode(hash, key, value, null);
        } else {
            Node<K, V> changeNode = null;
            if (currentNode.next != null && ((changeNode = getNode(key)) != null)) {
                table[index] = newNode(changeNode.hash, changeNode.key, value, currentNode);
                changeNode = table[index];
            } else {
                if (currentNode.hash == hash &&
                        (currentNode.key) == key || (key != null && key.equals(currentNode.key))) {
                    table[index] = newNode(currentNode.hash, currentNode.key, value, null);
                    changeNode = table[index];
                } else {
                    table[index] = newNode(hash, key, value, currentNode);
                }
            }
            if (changeNode != null) {
                V oldValue = changeNode.value;
                if (oldValue == null)
                    changeNode.value = value;
                return oldValue;
            }
        }
        ++size;
        return null;
    }

    private Node<K, V>[] resize() {
        Node<K, V>[] newTab = (Node<K, V>[]) new Node[DEFAULT_INITIAL_CAPACITY];
        return newTab;
    }

    private static int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }


    public void putAll(Map<? extends K, ? extends V> m) {
        putMapEntries(m);
    }

    private void putMapEntries(Map<? extends K, ? extends V> m) {
        for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
            K key = e.getKey();
            V value = e.getValue();
            put(key, value);
        }
    }

    public void clear() {
        if (table != null) {
            if (size > 0) {
                size = 0;
                Arrays.fill(table, null);
            }
        }
    }

    public Set<K> keySet() {
        Set<K> collection = new HashSet<>();
        for (Node<K, V> node : table) {
            while (node != null) {
                collection.add(node.key);
                node = node.next;
            }
        }
        return collection;
    }

    public Collection<V> values() {
        Collection<V> collection = new ArrayList<>();
        for (Node<K, V> node : table) {
            while (node != null) {
                collection.add(node.value);
                node = node.next;
            }
        }
        return collection;
    }

    public Set<Entry<K, V>> entrySet() {
        HashSet<Entry<K, V>> hash = new HashSet<>();
        for (Node<K, V> node : table) {
            while (node != null) {
                hash.add(node);
                node = node.next;
            }
        }
        return hash;
    }
}
