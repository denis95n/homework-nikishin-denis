package task02;

import task02.controller.server.Server;

public class MainServer {
    private static final int PORT = 1500;

    public static void main(String[] args) {
        init();
    }

    public static void init() {
        Server server = new Server(PORT);
        server.serverStart();
    }
}
