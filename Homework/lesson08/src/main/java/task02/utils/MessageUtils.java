package task02.utils;

import task02.bd.ConnectionUser;
import task02.bd.DataBaseConnection;

/**
 * Утилита отправки сообщений
 * Отправка работает по подключениям пользователей
 *
 * @see ConnectionUser
 * @author Nikishin
 */
public class MessageUtils {

    /**
     * Отправить сообщение текущему пользователю
     *
     * @param message сообщение
     * @param currentConnection кому отправить
     */
    public static void sendMessage(String message, ConnectionUser currentConnection) {
        currentConnection.getOutMessage().println(message);
    }

    /**
     * Отправить сообщение всем пользователям, кроме текущего
     *
     * @param message сообщение
     * @param currentConnection кому не отправлять
     */
    public static void sendMessageToAllUsers(String message, ConnectionUser currentConnection) {
        for (ConnectionUser connectionUser : DataBaseConnection.getConnection()) {
            if (!connectionUser.equals(currentConnection))
                sendMessage(message, connectionUser);
        }
    }

    /**
     * Печатает переданное сообщение в консоль
     *
     * @param message сообщение
     */
    public static void printMessage(String message) {
        System.out.println(message);
    }

    /**
     * Печатает переданную ошибку в консоль
     *
     * @param message сообшение
     */
    public static void printErrorMessage(String message) {
        System.err.println(message);
    }
}
