package task02;

import task02.controller.client.Client;

public class MainClient {
    private static final int PORT = 1500;
    private static final String HOST = "127.0.0.1";

    public static void main(String[] args) {
        init();
    }

    public static void init() {
        Client client = new Client(HOST, PORT);
        client.start();
    }
}
