package task02.bd;

import task02.utils.MessageUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Objects;

/**
 * Класс реализует обработку входных потоков от клиента
 * Ожидает запрос от клиента, обрабатывает его и отправляет ответ всем подключенным пользователям
 *
 * @author Nikishin
 * @see java.lang.Runnable
 */
public class ConnectionUser implements Runnable {
    private BufferedReader inMessage;
    private PrintWriter outMessage;
    private Socket userSocket;

    /**
     * Создаем входной и выходной поток для обработки отправки сообщений клиенту
     *
     * @param userSocket сокет подключенного к серверу клиента
     * @throws IOException ошибка получения потока, если сокет не создан
     */
    public ConnectionUser(Socket userSocket) {
        try {
            outMessage = new PrintWriter(userSocket.getOutputStream(), true);
            inMessage = new BufferedReader(new InputStreamReader(userSocket.getInputStream()));
            this.userSocket = userSocket;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public PrintWriter getOutMessage() {
        return outMessage;
    }

    private void resourceClose() {
        try {
            userSocket.close();
            inMessage.close();
            outMessage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Обработка входного потока и отправка нужного ответа клиенту
     *
     * @throws IOException ошибка при получении входного потока
     */
    @Override
    public void run() {
        try {
            String name = inMessage.readLine();
            String message;
            MessageUtils.sendMessage("Вы успешно пдключились к чату!", this);
            MessageUtils.sendMessageToAllUsers("[" + name + "] присоединился к чату!", this);

            while (true) {
                message = inMessage.readLine();
                if (message.equals("exit")) break;
                MessageUtils.sendMessageToAllUsers("[" + name + "]:" + message, this);
            }

            MessageUtils.sendMessageToAllUsers("[" + name + "] покинул чат!", this);
            DataBaseConnection.removeConnection(this);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            resourceClose();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConnectionUser)) return false;
        ConnectionUser that = (ConnectionUser) o;
        return Objects.equals(userSocket, that.userSocket);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userSocket);
    }
}
