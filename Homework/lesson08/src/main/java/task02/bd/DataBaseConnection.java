package task02.bd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Рееализует хранение всех подключений клиента к серверу
 * для дальнейшей отправки и обработки сообщений
 *
 * @see ConnectionUser
 * @author Nikishin
 */
public class DataBaseConnection {
    private static List<ConnectionUser> connectionUsers = Collections.synchronizedList(new ArrayList<>());

    /**
     * @return возвращает список всех подключений
     */
    public static List<ConnectionUser> getConnection() {
        return connectionUsers;
    }

    /**
     * Добавить новое подключение
     *
     * @param connection новое подключение
     */
    public static void addConnection(ConnectionUser connection) {
        connectionUsers.add(connection);
    }

    /**
     * @param connectionToRemove удалить подключение
     */
    public static void removeConnection(ConnectionUser connectionToRemove) {
        if (checkConnection(connectionToRemove)) {
            connectionUsers.remove(connectionToRemove);
        }
    }

    /**
     * Проверка есть ли уже такое подключение
     *
     * @param newConnection новое подключение
     * @return возращает boolean
     */
    public static boolean checkConnection(ConnectionUser newConnection) {
        return newConnection != null && connectionUsers.contains(newConnection);
    }
}
