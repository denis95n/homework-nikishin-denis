package task02.controller.server;

import task02.bd.ConnectionUser;
import task02.bd.DataBaseConnection;
import task02.utils.MessageUtils;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Этот класс создает сервер на порту
 * Обрабатывает клиентские запросы и возвращает ответ
 *
 * @author Nikishin
 */
public class Server {
    private ServerSocket serverSocket;
    private int port;

    /**
     * @param port порт для создания сервера
     */
    public Server(int port) {
        try {
            serverSocket = new ServerSocket(port);
            this.port = port;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Запуск сервера
     * Ожидает подключений от клиента и создает подключение в новом потоке
     * Добавляет подключение в базу всех подключений DataBaseConnection
     *
     * @see ConnectionUser
     * @see DataBaseConnection
     * @exception IOException ошибка при подключении
     */
    public void serverStart() {
        try {
            MessageUtils.printMessage("Сервер запущен на " + port + " порте.");

            while (true) {
                Socket clientSocket = serverSocket.accept();
                ConnectionUser connectionUser = new ConnectionUser(clientSocket);
                DataBaseConnection.addConnection(connectionUser);
                new Thread(connectionUser).start();

            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}