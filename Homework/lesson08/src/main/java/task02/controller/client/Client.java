package task02.controller.client;

import task02.utils.MessageUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Этот класс создает подключение к серверу через Socket.
 * Содержит входной и выходной поток, для общения с сервером
 *
 * @author Nikishin
 */
public class Client {
    private Socket clientSocket;
    private BufferedReader inMessage;
    private PrintWriter outMessage;
    private InputMessage inputMessage;

    /**
     * @param host адрес хоста
     * @param port порт для подключения к серверу
     * @exception IOException ошибка создания сокета, если нет включеного сервера по заданому порту
     * @exception IOException ошибка получения потока, если сокет не создан
     */
    public Client(String host, int port) {
        try {
            clientSocket = new Socket(host, port);
            inMessage = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            outMessage = new PrintWriter(clientSocket.getOutputStream(), true);
            inputMessage = new InputMessage();
        } catch (IOException e) {
            MessageUtils.printErrorMessage("Сервер не включен!");
        }
    }

    /**
     * Старт работы клиента
     * Отправка сообщений от пользователя на сервер
     * Создает поток внутренего класса InputMessage
     *
     * @see InputMessage
     */
    public void start() {

        try (Scanner scanner = new Scanner(System.in)) {
            inputMessage.start();
            MessageUtils.printMessage("Введите свое имя:");
            String name = scanner.nextLine();
            outMessage.println(name);
            String message = "";

            while (!message.equals("exit")) {
                message = scanner.nextLine();
                outMessage.println(message);
            }
            inputMessage.setStop();
        } finally {
            closeResource();
        }
    }

    private void closeResource (){
        try {
            inMessage.close();
            outMessage.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Внутрений класс отвечает за обработку сообщений от сервера и вывод на экран
     * с помощью входного потока полученного через сокет клиента.
     * Наследуется от Thread.
     *
     * @see java.lang.Thread
     * @exeption IOException ошибка при получении входного потока
     */
    private class InputMessage extends Thread {
        private boolean stop;

        public void setStop() {
            stop = true;
        }

        @Override
        public void run() {
            try {
                while (!stop) {
                    String str = inMessage.readLine();
                    MessageUtils.printMessage(str);
                }
            } catch (IOException e) {
                MessageUtils.printErrorMessage("Ошибка при получении сообщения.");
                e.printStackTrace();
            }
        }
    }
}
