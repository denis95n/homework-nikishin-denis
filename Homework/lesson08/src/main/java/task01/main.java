package task01;

import task01.coder.CodeBuilder;
import task01.loader.ExecuteCode;
import task01.loader.MyClassLoader;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

public class main {
    public static void main(String[] args) {
        File file = new File("lesson08\\src\\main\\java\\task01\\model\\SomeClass.java");
        CodeBuilder codeBuilder = new CodeBuilder();
        codeBuilder.writeCode();
        String code = codeBuilder.getCode();
        ExecuteCode.createClassToFile(file, code);

        File fileClass = new File("lesson08\\src\\main\\java\\task01\\model\\SomeClass.class");
        if(fileClass.isFile()) {
            try {
                MyClassLoader classLoader = new MyClassLoader(fileClass.getPath());
                Class clazz = classLoader.loadClass(fileClass.getPath());
                Object object = clazz.getConstructor().newInstance();
                clazz.getMethod("doWork").invoke(object);
            } catch (ClassNotFoundException | NoSuchMethodException
                    | InstantiationException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        } else throw new IllegalArgumentException("Набран неверный код");

    }
}
