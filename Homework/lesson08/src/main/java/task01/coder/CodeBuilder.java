package task01.coder;

import java.util.Scanner;
public class CodeBuilder {
    private StringBuilder stringBuilder;

    public String getCode() {
        return stringBuilder.toString();
    }

    public CodeBuilder() {
        this.stringBuilder = new StringBuilder();
    }

    public void writeCode() {
        Scanner scanner = new Scanner(System.in);
        String command;
        addStartCode();
        do {
            command = scanner.nextLine();
            stringBuilder.append(command).append("\n");
        } while (!command.equals(""));
        scanner.close();
        addEndCode();
    }

    private void addStartCode(){
        stringBuilder.append("package task01.model;").append("\n")
                .append("public class SomeClass implements Worker{").append("\n")
                .append("@Override").append("\n")
                .append("public void doWork() {").append("\n");
    }

    private void addEndCode(){
        stringBuilder.append("}").append("\n").append("}");
    }
}
