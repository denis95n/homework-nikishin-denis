package task01.loader;


import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MyClassLoader extends ClassLoader{
    private String nameClass;

    public MyClassLoader(String nameClass) {
        this.nameClass = nameClass;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        if (name.equals(nameClass)) {
            try (InputStream stream = new FileInputStream(name)) {
                byte[] bytes = IOUtils.toByteArray(stream);
                stream.close();
                return defineClass(null, bytes, 0, bytes.length);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return super.loadClass(name);
    }
}
