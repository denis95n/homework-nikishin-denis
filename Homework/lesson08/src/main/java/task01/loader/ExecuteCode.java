package task01.loader;

import javax.tools.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;

public class ExecuteCode {

    public static void createClassToFile(File file, String code) {
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(code);
            fileWriter.flush();
            fileWriter.close();
            compileFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void compileFile(File file) {
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);
        Iterable<? extends JavaFileObject> compilationUnit
                = fileManager.getJavaFileObjectsFromFiles(Collections.singletonList(file));
        JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager,
                diagnostics, null, null, compilationUnit);
        task.call();
    }
}
