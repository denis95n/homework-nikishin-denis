import server.Server;

public class Main {
    public static void main(String[] args) {
        try {
            if (args.length == 1) {
                String directory = args[0];
                new Server(directory).start();
            } else throw new IllegalArgumentException("Неверное число, входных параметров!");
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
        }
    }
}