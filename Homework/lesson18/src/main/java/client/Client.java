package client;

import response.StatusCode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Client extends Thread {
    private Socket clientSocket;
    private String directory;

    public Client(Socket clientSocket, String directory) {
        this.clientSocket = clientSocket;
        this.directory = directory;
    }

    @Override
    public void run() {
        try (InputStream in = clientSocket.getInputStream();
             OutputStream out = clientSocket.getOutputStream()) {
            String url = getRequestUrl(in);
            Path path = Paths.get(this.directory + url);
            if (Files.isDirectory(path)) {
                byte[] filesByte = getFilesByte(path);
                sendHTTPData(out, StatusCode.OK.getCode(), StatusCode.OK.getMessage(), filesByte.length);
                out.write(filesByte);
            } else {
                Optional<String> line = Files.lines(Paths.get("notfound.txt")).findFirst();
                sendHTTPData(out, StatusCode.NOT_FOUND.getCode(), line.get(),
                        line.get().length());
                out.write(line.get().getBytes());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getRequestUrl(InputStream input) throws NoSuchElementException {
        Scanner scanner = new Scanner(input).useDelimiter("\r\n");
        String line = scanner.next();
        return line.split(" ")[1];
    }

    private byte[] getFilesByte(Path pathDirectory) throws IOException {
        List<Path> listFilePaths = Files.list(pathDirectory).collect(Collectors.toList());
        StringBuilder filePaths = new StringBuilder();

        for (Path path : listFilePaths) {
            filePaths.append(path.getFileName()).append("\n");
        }

        return filePaths.toString().getBytes();
    }

    private void sendHTTPData(OutputStream output, int statusCode, String statusText, long length) {
        PrintStream printStream = new PrintStream(output, true);
        printStream.printf("HTTP/1.1 %s %s%n", statusCode, statusText);
        printStream.printf("Content-Length: %s%n%n", length);
    }
}
