package task01;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class Reflection {

    public static void cleanUp(Object object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput) {
        try {
            if (object instanceof Map) {
                Map<String, String> map = (Map<String, String>) object;
                cleanAndConvertMapFields(map, fieldsToCleanup, fieldsToOutput);
            } else {
                cleanAndConvertFields(object, fieldsToCleanup, fieldsToOutput);
            }
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private static void cleanAndConvertMapFields(Map<String, String> map, Set<String> fieldsToCleanup,
                                                 Set<String> fieldsToOutput) throws IllegalArgumentException {
        checkMapFieldsToExistence(map, fieldsToCleanup);
        checkMapFieldsToExistence(map, fieldsToOutput);

        fieldsToCleanup.forEach(map::remove);
        fieldsToOutput.forEach(field -> System.out.println(map.get(field)));
    }

    private static void cleanAndConvertFields(Object object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput)
            throws IllegalArgumentException {
        checkFieldsToExistence(object, fieldsToCleanup);
        checkFieldsToExistence(object, fieldsToOutput);

        fieldsToCleanup.forEach(s -> {
            try {
                Field field = object.getClass().getDeclaredField(s);
                setAccessible(field);
                defineTypeAndClearTo(field, object);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        });

        fieldsToOutput.forEach(s -> {
            try {
                Field field = object.getClass().getDeclaredField(s);
                setAccessible(field);
                String fieldValue = convertToString(field, object);
                System.out.println(fieldValue);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        });
    }

    private static void defineTypeAndClearTo(Field field, Object object) throws IllegalAccessException {
        switch (field.getType().getName()) {
            case "int":
            case "short":
            case "long":
            case "byte":
                field.set(object, (byte) 0);
                break;
            case "float":
                field.set(object, 0.0f);
                break;
            case "double":
                field.set(object, 0.0d);
                break;
            case "char":
                field.set(object, '0');
                break;
            default:
                field.set(object, null);
        }
    }

    private static String convertToString(Field field, Object object) throws IllegalArgumentException {
        try {
            if (field != null) {
                setAccessible(field);
                return field.get(object) != null ? field.get(object).toString() : null;
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        throw new IllegalArgumentException("Ошибка доступа!");
    }

    private static void checkFieldsToExistence(Object object, Set<String> fieldSet)
            throws IllegalArgumentException {
        Field[] fields = object.getClass().getDeclaredFields();
        List<String> fieldsName = new ArrayList<>();
        Arrays.stream(fields).forEach(field -> fieldsName.add(field.getName()));
        List<String> noFoundFields = fieldSet.stream()
                .filter(s -> !fieldsName.contains(s))
                .collect(Collectors.toList());

        if (noFoundFields.size() != 0) {
            throw new IllegalArgumentException("Не найдены поля: " + noFoundFields.toString());
        }
    }

    private static void checkMapFieldsToExistence(Map<String, String> map, Set<String> fieldsSet)
            throws IllegalArgumentException {
        List<String> noFoundFields = fieldsSet.stream()
                .filter(s -> !map.containsKey(s))
                .collect(Collectors.toList());

        if (noFoundFields.size() != 0) {
            throw new IllegalArgumentException("Не найдены поля: " + noFoundFields.toString());
        }
    }

    private static void setAccessible(Field field) {
        if (!field.isAccessible()) {
            field.setAccessible(true);
        }
    }
}
