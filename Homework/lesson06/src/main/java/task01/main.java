package task01;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class main {
    public static void main(String[] args) {
        Set<String> fieldsToCleanup = new HashSet<>(Arrays.asList("age", "name"));
        Set<String> fieldsToOutput = new HashSet<>(Arrays.asList("age", "name"));
        Person person = new Person("Den", 25);
        Reflection.cleanUp(person, fieldsToCleanup, fieldsToOutput);
    }
}
