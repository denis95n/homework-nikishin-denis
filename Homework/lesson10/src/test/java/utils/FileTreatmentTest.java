package utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class FileTreatmentTest {
    private Path path;
    private Path pathToWrite;

    @BeforeEach
    void setUp() {
        path = Paths.get("src\\main\\resources\\startFile.txt");
        pathToWrite = Paths.get("src\\main\\resources\\endFile.txt");
    }

    @Test
    void readFileToArray() throws IOException {
        assertNotNull(FileTreatment.readFileToArray(path));
    }

    @Test
    void writeArrayToFile() throws IOException {
        int[][] matrix = FileTreatment.readFileToArray(path);
        FileTreatment.writeArrayToFile(matrix, pathToWrite);
    }
}