package game;

import concurrent.MyPhaser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.FileTreatment;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GameOfLifeTest {
    private GameOfLife gameOfLife;

    @BeforeEach
    void setUp() throws IOException {
        Path path = Paths.get("src\\main\\resources\\startFile.txt");
        int[][] matrix = FileTreatment.readFileToArray(path);
        gameOfLife = new GameOfLife(matrix);
    }

    @Test
    public void getCountLifeCellsAroundTo(){
        int expectedCount = 2;
        int actualCount = gameOfLife.getCountLifeCellsAroundTo(1,4);
        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void compareGames(){
        MyPhaser myPhaser = new MyPhaser(1, 400);
        Instant start = Instant.now();
        new GameProgressRunnable(gameOfLife, 0, 40, myPhaser);
        new GameProgressRunnable(gameOfLife, 40,
                    80, myPhaser);
        new GameProgressRunnable(gameOfLife, 80,
                120, myPhaser);
        new GameProgressRunnable(gameOfLife, 120,
                gameOfLife.getMatrix().length, myPhaser);
        while (!myPhaser.isTerminated()) {
            myPhaser.arriveAndAwaitAdvance();
        }
        Instant finish = Instant.now();
        System.out.println("Четыре потока: " + Duration.between(start, finish).toMillis() + "мс");

        start = Instant.now();
        new GameProgress(gameOfLife, 200).startGame();
        finish = Instant.now();
        System.out.println("Один поток: " + Duration.between(start,finish).toMillis() + "мс");
    }
}