package concurrent;

import java.util.concurrent.Phaser;

public class MyPhaser extends Phaser {
    int numPhases;

    public MyPhaser(int parties, int numPhases) {
        super(parties);
        this.numPhases = numPhases;
    }

    @Override
    protected boolean onAdvance(int p, int regParties) {
        return p == numPhases || regParties == 0;
    }
}
