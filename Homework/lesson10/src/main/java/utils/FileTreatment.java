package utils;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileTreatment {
    private final static String TXT = "txt";

    public static int[][] readFileToArray(Path filePath) throws IOException {
        int[][] matrix;
        checkFileToRead(filePath);
        checkFileToCorrect(filePath);
        List<String> fileLines = Files.lines(filePath).collect(Collectors.toList());
        int columns = fileLines.get(0).length();
        int rows = fileLines.size();
        matrix = new int[rows][columns];
        int row = 0;
        for (String line : fileLines) {
            int column = 0;
            for (char ch : line.toCharArray()) {
                matrix[row][column] = Character.digit(ch, 10);
                column++;
            }
            row++;
        }

        return matrix;
    }

    public static void writeArrayToFile(int[][] matrix, Path path) throws IOException {
        if (matrix.length > 0) {
            try (FileWriter writer = new FileWriter(path.toString(), false)) {
                for (int row = 0; row < matrix.length; row++) {
                    writer.write(Arrays.toString(matrix[row]));
                    writer.append("\n");
                }
                writer.flush();
            }
        } else throw new IllegalArgumentException("Матрица пустая!");
    }

    private static String getFileExtension(Path path) {
        String name = path.getFileName().toString();
        int extensionStart = name.lastIndexOf(".");
        return extensionStart == -1 ? "" : name.substring(extensionStart + 1);
    }

    private static void checkFileToRead(Path path) throws IllegalArgumentException {
        if (!Files.isExecutable(path)) {
            throw new IllegalArgumentException("Неверный путь! " + path.toString());
        } else {
            String extension = getFileExtension(path);
            if (!extension.equals(TXT)) {
                throw new IllegalArgumentException("Неверный тип файла! Файл должен быть типа: \"" + TXT + "\"");
            }
        }
    }

    private static void checkFileToCorrect(Path path) throws IOException, IllegalArgumentException {
        List<String> fileLines = Files.lines(path).collect(Collectors.toList());
        if (fileLines.size() > 0) {
            int columns = fileLines.get(0).length();
            for (String line : fileLines) {
                if (columns == line.length()) {
                    for (char ch : line.toCharArray()) {
                        int num = Character.digit(ch, 10);
                        if (!(num == 0 || num == 1)) {
                            throw new IllegalArgumentException("В файле могут быть только [0 - 1]");
                        }
                    }
                } else throw new IllegalArgumentException("Не корректный файл!");
            }
        } else throw new IllegalArgumentException("Файл пустой!");
    }
}
