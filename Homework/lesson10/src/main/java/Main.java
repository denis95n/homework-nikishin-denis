import game.GameOfLife;
import game.GameProgressRunnable;
import concurrent.MyPhaser;
import utils.FileTreatment;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {
    final static int NUMBER_PHASER_ITERATION_IN_ONE_MOVE = 2;

    public static void main(String[] args) {
        try {
            if (args.length != 3) {
                throw new IllegalArgumentException("Не верное количество аргументов, должно быть: \n" +
                        "Первый аргумент - путь входного файла\n" +
                        "Второй аргумент - имя выходного файла\n" +
                        "Третий аргумент - количество шагов");
            } else {
                Path startFilePath = Paths.get(args[0]);
                Path endFilePath = Paths.get(args[1]);
                int countStep = Integer.parseInt(args[2])-1;
                int[][] matrix = FileTreatment.readFileToArray(startFilePath);
                int middleSectionIndex = getMiddleLengthMatrix(matrix.length);
                MyPhaser myPhaser = new MyPhaser(1, countStep * NUMBER_PHASER_ITERATION_IN_ONE_MOVE);
                GameOfLife gameOfLife = new GameOfLife(matrix);
                new GameProgressRunnable(gameOfLife, 0, middleSectionIndex, myPhaser);
                new GameProgressRunnable(gameOfLife, middleSectionIndex, matrix.length, myPhaser);
                while (!myPhaser.isTerminated()) {
                    myPhaser.arriveAndAwaitAdvance();
                }

                FileTreatment.writeArrayToFile(gameOfLife.getMatrix(), endFilePath);
                System.out.println("Синхронизатор фаз завершен");
            }
        } catch (NumberFormatException ex) {
            System.err.println("ОШИБКА! " + ex.getMessage() + " не является числом!");
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int getMiddleLengthMatrix(int lengthMatrix) {
        return (int) Math.ceil(lengthMatrix / 2.0);
    }
}
