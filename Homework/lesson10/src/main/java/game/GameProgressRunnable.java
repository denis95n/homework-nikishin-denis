package game;

import concurrent.MyPhaser;

public class GameProgressRunnable implements Runnable {
    private MyPhaser phaser;
    private int startIndexSection;
    private int endIndexSection;
    private GameOfLife gameOfLife;

    public GameProgressRunnable(GameOfLife gameOfLife, int startIndexSection, int endIndexSection, MyPhaser phaser) {
        this.gameOfLife = gameOfLife;
        this.startIndexSection = startIndexSection;
        this.endIndexSection = endIndexSection;
        this.phaser = phaser;
        phaser.register();
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (!phaser.isTerminated()) {
            int countLifeCellAround;
            boolean currentCellIsAlive;
            int[][] gameFiled = gameOfLife.getMatrix();
            int[][] gameFieldCopy = new int[gameFiled.length][gameFiled[0].length];
            for (int x = startIndexSection; x < endIndexSection; x++) {
                for (int y = 0; y < gameFieldCopy[x].length; y++) {
                    currentCellIsAlive = gameOfLife.checkCellAlive(x, y);
                    countLifeCellAround = gameOfLife.getCountLifeCellsAroundTo(x, y);
                    if (currentCellIsAlive) {
                        if (countLifeCellAround < 2 || countLifeCellAround > 3) {
                            gameFieldCopy[x][y] = GameOfLife.getCellDeath();
                        } else gameFieldCopy[x][y] = GameOfLife.getCellAlive();
                    } else if (countLifeCellAround == 3) {
                        gameFieldCopy[x][y] = GameOfLife.getCellAlive();
                    }
                }
            }
            phaser.arriveAndAwaitAdvance();
            gameOfLife.setMatrixToSection(gameFieldCopy, startIndexSection, endIndexSection);
            phaser.arriveAndAwaitAdvance();
        }
    }
}
