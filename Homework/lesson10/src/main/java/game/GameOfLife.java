package game;

import java.util.ArrayList;
import java.util.List;

public class GameOfLife {
    private int[][] matrix;
    private static final int CELL_ALIVE = 1;
    private static final int CELL_DEATH = 0;

    public GameOfLife(int[][] matrix) {
        this.matrix = matrix;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public void setMatrixToSection(int[][] matrixCopy, int cellStart, int countLine) {
        for (int x = cellStart; x < countLine; x++) {
            for (int y = 0; y < matrixCopy[x].length; y++) {
                this.matrix[x][y] = matrixCopy[x][y];
            }
        }
    }

    public static int getCellAlive() {
        return CELL_ALIVE;
    }

    public static int getCellDeath() {
        return CELL_DEATH;
    }

    private int getIndexIncrementX(int index){
        return ++index == matrix.length ? 0 : index;
    }

    private int getIndexDecrementX(int index){
        return --index < 0 ? matrix.length - 1 : index;
    }

    private int getIndexIncrementY(int index){
        return ++index == matrix[0].length ? 0 : index;
    }

    private int getIndexDecrementY(int index){
        return --index < 0 ? matrix[0].length - 1 : index;
    }

    public int getCountLifeCellsAroundTo(int currentX, int currentY){
        int countLifeCells = 0;
        List<Integer> yCoordinateList = getYCoordinateList(currentY);
        List<Integer> xCoordinateList = getXCoordinateList(currentX);

        for(int x : xCoordinateList) {
            for (int y : yCoordinateList) {
                if (checkCellAlive(x, y) && !(x ==  currentX && y == currentY)) {
                    countLifeCells++;
                }
            }
        }

        return countLifeCells;
    }

    private List<Integer> getYCoordinateList(int cellY) {
        return new ArrayList<Integer>() {{
            add(getIndexDecrementY(cellY));
            add(cellY);
            add(getIndexIncrementY(cellY));
        }};
    }

    private List<Integer> getXCoordinateList(int cellX) {
        return new ArrayList<Integer>() {{
            add(getIndexDecrementX(cellX));
            add(cellX);
            add(getIndexIncrementX(cellX));
        }};
    }

    public boolean checkCellAlive(int x, int y){
        return matrix[x][y] == CELL_ALIVE;
    }

    @Override
    public String
    toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("-----GameOfLife-----").append("\n");

        for (int[] ints : matrix) {
            for (int anInt : ints) {
                sb.append(anInt).append(" ");
            }
            sb.append("\n");
        }

        sb.append("-------------------").append("\n\n");
        return sb.toString();
    }
}
