package game;

public class GameProgress {
    private int countStep;
    private GameOfLife gameOfLife;

    public GameProgress(GameOfLife gameOfLife, int countStep) {
        this.gameOfLife = gameOfLife;
        this.countStep = countStep;
    }

    public void startGame() {
        int countLifeCellAround;
        boolean currentCellIsAlive;
        int[][] gameField = gameOfLife.getMatrix();
        int[][] gameFieldCopy = new int[gameField.length][gameField[0].length];
        for (int step = 0; step < countStep; step++) {
            for (int x = 0; x < gameFieldCopy.length; x++) {
                for (int y = 0; y < gameFieldCopy[x].length; y++) {
                    currentCellIsAlive = gameOfLife.checkCellAlive(x, y);
                    countLifeCellAround = gameOfLife.getCountLifeCellsAroundTo(x, y);
                    if (currentCellIsAlive) {
                        if (countLifeCellAround < 2 || countLifeCellAround > 3) {
                            gameFieldCopy[x][y] = GameOfLife.getCellDeath();
                        } else gameFieldCopy[x][y] = GameOfLife.getCellAlive();
                    } else if (countLifeCellAround == 3) {
                        gameFieldCopy[x][y] = GameOfLife.getCellAlive();
                    }
                }
            }
            gameOfLife.setMatrixToSection(gameFieldCopy, 0, gameFieldCopy.length);
        }
    }
}
